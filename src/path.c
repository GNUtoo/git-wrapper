/* Copyright (C) 2022 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "git.h"

#define DEBUG 0

#if DEBUG == 1
bool print_repository(struct repository *repository)
{
	printf("{\n"
	       "\tprotocol: %d\n"
	       "\tdomain[%d]: %s\n"
	       "\tpath[%d]: %s\n"
	       "}\n",
	       repository->protocol,
	       repository->domain_len,
	       repository->domain,
	       repository->path_len,
	       repository->path);

}
#endif

bool extract_all(char *repository, struct repository *result)
{
	char *end;
	char *start;
	ssize_t offset = 0;

	if (strlen(repository) < 1)
		return false;

	start = repository;

	if (!strncmp("git://", start, strlen("git://"))) {
		result->protocol_len = strlen("git://");
		result->protocol = GIT;
		offset = result->protocol_len;
	} else if (!strncmp("http://", start, strlen("http://"))) {
		result->protocol_len = strlen("http://");
		result->protocol = HTTP;
		offset = result->protocol_len;
	} else if (!strncmp("https://", start, strlen("https://"))) {
		result->protocol_len = strlen("https://");
		result->protocol = HTTPS;
		offset = result->protocol_len;
	} else {
		return false;
	}

	start = repository + offset;
	if (start >= (repository + strlen(repository) - 1))
		return false;

	end = strchr(start, '/');
	if ((end == NULL) || (end == repository + strlen(repository)))
		return false;

	result->domain_len = end - start;
	result->domain = calloc(1, result->domain_len + 1);
	if (!result->domain)
		return false;

	memcpy(result->domain, start, result->domain_len);

	start = end + 1;
	if (start >= (repository + strlen(repository) - 1))
		goto free_domain;

	end = repository + strlen(repository);
	if ((end - start) > 4) {
		if (!strncmp(".git",
			 repository + strlen(repository) - 4,
			 strlen(".git"))) {
			end = repository + strlen(repository) - 4;
		}
	}

	result->path_len = end - start;
	result->path = calloc(1, result->path_len + 1);
	if (!result->path)
		goto free_domain;

	memcpy(result->path, start, result->path_len);

	return true;

free_domain:
	free(result->domain);

	return false;
}

void free_repository(struct repository *repository)
{
	assert(repository);

	free(repository->path);
	free(repository->domain);
}

char *compute_path(char *repository)
{
	ssize_t len;
	struct repository result;
	char *path;
	bool ok;

	ok = extract_all(repository, &result);
	if (!ok) {
		path = calloc(1, 2);
		if (path == NULL)
			return NULL;
		strcpy(path, "");
		return path;
	}

	len = snprintf(NULL, 0, "/srv/git/%s/%s.git",
		result.domain, result.path);

	path = calloc(1, len + 1);
	if (path == NULL) {
		free_repository(&result);
		return NULL;
	}

	len = snprintf(path, len + 1, "/srv/git/%s/%s.git",
		result.domain, result.path);

	free_repository(&result);
	return path;
}
